% !TEX root = ../dissertation.tex

With the rapid adoption and spread of the Internet, computers and the Internet have become indispensable to society. 
In many parts of the world, very little business is done without some use of the Internet. 
The Internet contains an expansive collection of human knowledge and enables communication that was unthinkable a few decades ago.
Social networks play a role in how people interact with each other.
The systems that constitute the Internet as we know it are massive and critical to daily life.

Unfortunately, large systems that do not consider a person's agency can treat that person poorly. 
Perhaps unsurprisingly due to the importance of the Internet, this can be a challenge for systems built on the Internet.
Users are tracked and have personal information inferred. Often this happens without asking the user, and when the user is asked, the models of consent do not hold up to scrutiny~\cite{van2012online}.
What information users access is shaped by these inferences and governments in unaccountable ways.

Three of the technical properties that enable user agency are transparency, trust, and privacy. 
Transparency allows the user to observe the actions of the system so that they may understand the impacts of their behavior.
Trust allows the user to control the parties that have a privileged position in acting as their agent or providing them information.
Privacy allows the user to make choices freely and can be defined as a choice of when to be observed.
Each of these technical properties enable user decision making.

When a system is Internet-based, several concerns are amplified. 
The scope of the system can grow almost arbitrarily, and beyond the intent of its creator~\cite{leeregret}. 
Whether a system is designed to help or harm particular users is not as important as its emergent behavior.
In this way, Internet measurement is a singularly useful tool to gain information about Internet-based systems; it analyzes precisely what the system is currently doing.
This information can be shared directly, used to assert axioms of the system's design, or help users blend into a crowd.

In this dissertation, I explore the use of Internet measurement for system transparency, trust, and privacy through four case studies.
First, I explore the application of Internet measurement to Internet censorship to provide transparency to a deliberately opaque practice.
Second, I further explore Internet censorship and the use of Internet measurement in aiding censorship circumvention.
Third, I apply Internet measurement to the evaluation of Web tracker-blocking to produce easily digestible recommendations and advice for privacy-enhancing tool designers.
Fourth, I use multiple perspectives of Internet measurement to illuminate the extent of transparency in HTTPS certificates, improving transparency for the average website administrator and providing insight for improving Internet measurement.

\section{Measuring Internet Censorship}

Internet censorship is a phenomenon that has been studied from multiple perspectives, technical and non-technical, in recent years~\cite{censoredplanet,zittrain2003internet,clayton2006ignoring,aryan2013internet,Chaabane2014a,anon2014towards,winters12foci,Dalek2013a,jones14imc,mackinnon2009china,knockel2011three,crandall2013chat,fifieldtorblocks,oniKR,oniJO,freedomhouse2016,stateGhana2016}.
At its core, a user attempts to use services outside of its network that the network operator does not want to be used.
This phenomenon is often discussed in the more specific context of citizens attempting to visit websites or parts of the Internet that the government forbids.
Governments don't disclose what they are blocking, and often provide only vague descriptions of what is being censored~\cite{fud}.
This leads to self-censorship and an inability for users to engage in an informed discussion on the policy.

In recent years, measurement of censorship has developed techniques that no longer rely on an extensive network of in-country volunteers to maintain censorship probes~\cite{ooni}.
Rather, researchers developed techniques that make use of existing network infrastructure to measure censorship~\cite{scott2016satellite,pearce2017global,pearceaugur,ensafi2014detecting}.
I identified a simple technique to measure commonly used application-layer censorship that takes advantage of legacy services online on the Internet today.
This technique, combined with simple heuristics, is able to identify national-level censorship in practice.

This study included the largest test list of web sites for the purpose of detecting censorship, testing 100,000 web sites across 40 countries.
Additionally, we tested a smaller list of sensitive domains across 82 countries, and were able to quantify and provide qualitative descriptions of censorship to support ONI and Freedom House reports.
This technique, and its evolution in future work, provide one of the key technical aspects of Censored Planet, a longitudinal censorship measurement platform.
Censored Planet, and the transparency it provides, were important in the discussion surrounding Kazakhstan's recent deployment of censorship tools.
Users and stakeholders in the system alike can simply look at a list of blocked domains retrieved using these tools, rather than trusting the censoring countries' claims about what they are censoring and why.

\section{Measuring to Circumvent Censorship}

Deployment of censorship circumvention tools can benefit from Internet measurement techniques.
At time of writing, the first continuous deployment of Refraction Networking, a next generation censorship circumvention technique, is available and being used by users in censored countries.
In order to deploy this technique, a key step was to discover which websites may be used to provide plausible deniability for users in each country based upon our deployment and network conditions.
To provide this list, we must understand which extant websites place our deployed network appliance on path between a typical user in a censoring country and an uncensored website.
In order to do this we use Internet measurement.

We tested a deployment in the spring of 2017 on a trial basis, which I present the results of. 
Over a single week, 50,000 unique daily users were able to access the Internet through our deployment, which has translated to over 100,000 daily users in our current deployment.
This led to our continuous deployment with 500,000 users that we are operating, and has been up for 18 consecutive months. 
During our continuous operation, we have observed that during censorship events our system is relied more heavily upon by users.
This shows promise for Refraction Networking's use as a more trustworthy fallback technique, to keep users online during censorship events.

\section{Measuring Web Tracking Evasion}

Web tracking is a system in which end-users are often unwitting participants~\cite{ur2012smart,  chanchary2015user, yao2017folk, melicher2016not}. 
Third-parties are requested in the background when users visit web pages and these third parties share that information further still, all while building a profile of each user.
Users have varied degrees of discomfort with this, depending especially on what sorts of inferences the trackers make, medical and religious information being most sensitive~\cite{dolin2018unpacking} 
Several browser extensions have been created to help preserve user privacy, and privacy advocates often recommend them~\cite{protonRecommendations}.
However, researchers lacked quantitative justification for specific recommendations of which tools to install and an understanding of why that tool worked better.

In this study, I first develop a platform for evaluating web tracking across a top-million site list. 
This is larger than prior work that focused on third-party information sharing, and required a more accurate model of the tools that would block web tracking.
I used analysis of blocking extension performance, their performance without Acceptable Ads programs enabled, and qualitative descriptors of the extensions to identify which factors were indicators of the strongest tools.

From these experiments, we now know that organizational mission is the strongest indicator of an extension's efficacy. 
Users have good reason to mistrust ad blockers' incidental claims to privacy benefits and should focus on privacy-first tools.
Developers of these tools also can learn that a focus on high-impact targets will provide more benefit to users at lower cost than expansive block-lists.

\section{Measuring the Certificate Ecosystem}

The last system I measure in this dissertation is the HTTPS certificate ecosystem.
Encryption has become more popular as a default means of protecting information transmitted over the Internet, and on the Web that encryption is rooted in certificates.
More specifically, a website proves its identity to users by providing cryptographic proof, a certificate, that its identity a trusted third party verifies.
One common way this system fails is the misissuance of these certificates by the trusted third parties: someone other than the operator of \texttt{example.com} obtaining a certificate for \texttt{example.com}~\cite{amann2017mission}.
Such a failure undermines the security of a website when the website operator does not know such a thing has happened.

In this study, I analyze the HTTPS certificate ecosystem from a variety of perspectives, providing the most complete view of all certificates to date.
This involved developing new scanning methodologies, i.e. zonefile scanning and CT domain scanning, and comparing passive measurements from the ICSI Notary~\cite{notarypaper} to a diverse set of scanning methodologies for the first time.
We also merged all results from our techniques into ongoing public data sets.

This has helped the average website administrator more easily and completely monitor for misissued certificates, as well as protect that same administrator from errantly issuing certificates.
Beyond helping just system administrators, this also helped researchers in Internet measurement better understand the biases in their methodology to help them better serve system administrators.
This helps improve the Web's roots of trust.


\paragraph{Thesis Statement.} 
\thesisStatement

\paragraph{Structure.} 
In this dissertation, I demonstrated this by exploring Internet censorship, Web tracker-blocking, and the certificate ecosystem. 
In Chapters II \& III, I explore the application of Internet measurement to Internet censorship, both in providing transparency to a deliberately opaque practice and in aiding censorship circumvention.
In Chapter IV, I apply Internet measurement to the evaluation of Web tracker-blocking to produce easily digestible recommendations for laypeople and advice for privacy-enhancing tool designers.
In Chapter V, I use multiple perspectives of Internet measurement to illuminate the extent of transparency in HTTPS certificates, improving transparency for the average website administrator, providing insight for improving Internet measurement.
Finally, in Chapter VI I draw conclusions from these case studies and look forward to potential uses of Internet measurement for the direct benefit of end users.

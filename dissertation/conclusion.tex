% !TEX root = ../dissertation.tex
This dissertation has explored three separate software systems with security properties on the Internet: online censorship, web tracking, and TLS PKI.
We analyzed each of these with Internet measurement to give the end users transparency into censorship, privacy from trackers, and trust in PKI.
This has led to a more equitable Internet in several ways. 
Half of a million users have Refraction Networking clients in their hands as a way to circumvent censorship.
Browser policies that affect Kazakhstanis' access to the Internet were shaped by application-layer remote censorship measurement by the Censored Planet project.
Recommendations of which browser extension to use to defend user privacy can now be shaped by direct evidence.
Each of these are examples of how transparency, trust, and privacy can be enhanced with Internet Measurement.

Internet measurement provides insights that are not otherwise attainable, and in this dissertation I attribute this to four capabilities of Internet measurement.
First, Internet measurement enables discovery of hosts of a particular character for use, such as echo servers or filtering potential decoys.
Second, it enables insight into the topology of the network infrastructure, as used in identifying initial potential decoys.
Third, it enables measurement of populations and their behaviors on the Internet, such as websites and their use of third party resources.  
Finally, it enables identification or measurement of emergent properties, such as privacy impacts due to the common third parties online and biases introduced to TLS measurement through the SNI extension.

Throughout this dissertation, we have applied these four capabilities to a limited scope of problems users face.
However, there are far more problems users face online than we address here; challenges to an equitable Internet are not limited to those in this dissertation, and seem to be creeping in scope.
In the remainder of this thesis, I will identify some ways I see potential in the application of these properties of Internet measurement to improving the Internet for end users.

One direction of future research is study of the quality of Internet access. 
Prior work~\cite{mcdonald2018403} has demonstrated that from a global, site-level perspective, the Internet is not equally accessible.
This leads to many questions that confront more subtle aspects of this phenomenon.
Can we observe different websites blocking beyond just the homepage, such as not permitting checkout when a billing address is in a sanctioned country?
Can we observe blocking based on geographic location between different parts of a single country?
Moreover, this work elides the question of quality of Internet connection.
In particular, within a single country what are the differences between under-served and adequately served regions in Internet quality and how does this affect quality of service for essential service page loads?
If Internet quality is quantified in terms of up-time, latency, and bandwidth, these are population properties that Internet measurement could be applied to measure.

Another direction of future research is to more accurately model users in the study of online tracking.
And improvement we made over prior work was to weight websites in a Zipfian manner to approximate site visit frequency.
However, this leaves many aspects of user experience omitted.
Can we integrate work that reasonably automates page traversal in a way that resembles user behavior to a browser instrumented for tracking detection?
Does this impact how trackers treat a user?
Additionally, our study occurred from a subnet known to perform scanning and does not typically have users.
Does tracking behavior become more representative of user experience when performed in typical user network locations?
Moreover, does your network location create a baseline belief in the advertiser models about your interests or value in advertising?

The final question of each of the last two proposed research directions can be applied together to gain deep insight into discrimination via the Internet.
Consider a case study in the United States where access to the Internet is not a given and targeted advertisements have been found to be discriminatory.
These questions, answered quantitatively at a national level, could provide significant insight into emergent properties and help measure our progress in correcting them.

An additional direction of future research in privacy is to focus on the network level.
Prior work in enhancing Tor circuit selection algorithms~\cite{rochet2017waterfilling,edman2009awareness} has used topology to quantify effectiveness of privacy protections.
Understanding a comprehensive baseline using a more accurate user model, topology information, DNS leaks in recursive resolution, and centralized web hosting may be useful in the development of weaker, but more usable privacy enhancing technologies.
At very least these would help users understand the degree of exposure they experience on the Internet today.

In our work on Refraction Networking, we identified that a major impediment to dial speed was the frequency of stations not being on path between a given client and decoy.
An avenue of improvement for Refraction Networking performance is therefore performing router-level prediction of routes to reduce the dial failure frequency.
This is a challenge, not only in accurately predicting route choice at a fine-grained level, but also in performing this for each client without obtaining the client IP address.
Two potential solutions for the latter problem are immediately apparent, with trade-offs: developing a client IP address indexed prefix tree of decoys and shipping some data about routing to each client to allow them to make the decision.
The trade-off here is a question of how little data we can send to the client (reducing overhead), how accurate we can be (improving performance), and how much computation we force the client to do (improving performance).
This is to say nothing of the challenges in developing fine-grained route prediction when doing so at the AS level is still being improved~\cite{hyperpath}.

A direction of future research in Internet measurement that is not closely related to any chapter of this dissertation is the study of channels of radicalization. 
There is some prior work in understanding how information travels through fringe web communities~\cite{zannettou2018origins,mathew2019spread}, state sponsored behavior~\cite{zannettou2019let}, and the direction of users within fringe communities on YouTube~\cite{ribeiro2020auditing}.
However this area has many open questions. Some may be answered in part with Internet measurement, such as host discovery and web crawls.
How do platforms less centralized than YouTube direct users between communities?
Mastodon has been studied from a structural level~\cite{raman2019challenges}, but the content therein and how users move within communities has not seen similar focus.

Additionally, there are some directions of future research that may not require Internet measurement, but work in this dissertation points to.

The first is improvement in scalability of Refraction Networking. 
Conjure~\cite{conjure} already takes significant steps toward doing this, removing the hung open connection to the decoy, reducing the burden of hosting a refraction station. 
Future work could deploy Conjure for practical use or identify improvements that enable it for deployment at a Tier-1 ISP. 
A Tier-1 ISP deployment would cement the utility of Refraction Networking, due to the high number of decoys available.
Additionally, future work making Conjure resistant to traffic analysis, to the standard of Slitheen~\cite{slitheen16}, would be compelling; Refraction Networking fits in as a strong back-up circumvention technique.

The second is focused study of ad exchange behavior. In particular, precisely what information they leak to bidders, what techniques they use to fingerprint users, and how they handle opt-out behaviors.
We now know that a few dozen ad exchanges are responsible for the bulk of out-of-band information sharing in the web tracking ecosystem.
This is a small enough number that they can be analyzed manually, and better understand the major impact they have and how to better defend against them.
Additionally, constant monitoring of the important findings can be put in place with measurement techniques that prevent unknowns from growing underneath our knowledge.


This dissertation has been guided by improvements to users' transparency, trust, and privacy, particularly with Internet measurement.
While Internet measurement is a powerful tool to understand systems, it is not a panacea.
In particular, work of technical discovery must be matched by work to deploy and teach the lessons learned in order to improve privacy and transparency.
Thus, future work should consider how users are brought into the loop in its developments and lessons.
We should take deliberate steps to ensure that people are benefiting from what we do.








\begin{comment}
This dissertation has explored three separate software systems with security properties on the Internet: online censorship, web tracking, and TLS PKI.
We analyzed each of these with Internet measurement to give the end users transparency into censorship, privacy from trackers, and trust in PKI.
All of these properties are in the spirit of producing a more equitable Internet.

It is important to acknowledge the shortcomings of these steps, even if as future directions of research.
There is a gap in knowledge between the discoveries here and what a typical user reasons about.
Transparency measures are limited in their efficacy by the number of people that digest the new information.
Privacy is limited when experts do not clearly articulate risks in a way users would otherwise heed.
A trustworthy system gives a user even more work if they need to understand the system in great detail to reason about it.

I believe that future work, particularly focused on univeillance, has significant challenges in translating technical improvements to tangible ones.
Univeillance here is taken from Steve Mann's Veillance plane~\cite{mann2013veilance}. 
Univeillance is composed both of transparency into the doings of those with power, sousveillance, and improved privacy for those not in power, anti-surveillance.
This coherent and complementary view is useful in retrospect to understand the role of the work in this dissertation and direct future work.

%Trust and delegation are intrinsically linked?

\section{User Models}

In order to more accurately reflect user agency when pursuing univeillance, we may need a more accurate understanding of the user.
Simply asking a user to make every security decision is too burdensome for that user, and leads to fatigue~\cite{akhawe2013alice}. 
As such, it may be better to build a model of the users' security considerations and make decisions for them. 
This may also improve the users' trust, as a layman understands it, in acting as a delegate, and more accurately capture the users' cost of compromise.

Some work has looked to identify small sets of threat models for a single community, to some success~\cite{frik2019privacy}.
I expect this, when combined with automatic configuration, to prove a strong improvement for end users.
In the meantime, a restricted threat model and manual assistance have shown great promise in the context of Intimate Partner Violence, reframing computer security as a clinical intervention~\cite{havron2019clinical}.
While Clinical Computer Security does not scale to all users with manual intervention, the techniques are fundamentally very similar.

\section{Sousveillance}

User-directedness is similarly vital in work focused on sousveillance. 
Undersight is most useful when typical people understand the output of what they are seeing.
Therefore, outreach is is needed, particularly among those harmed by digital systems.
This leads to a compelling direction of work, relating historic patterns of discrimination to those perpetrated with large digital systems.
Leaders in communities that fought injustice have a foothold in understanding how these systems mistreat them now, and how they can be combated.
Systems of oppression are not new, and we, as computer scientists must not ignore the lessons of the past.

%e.g. housing and ad discrimination for black americans.
%Surveillance is fundamental to black experience in america, an so a possible inroad, and underserved community, exists!

\section{Anti-surveillance}

The work in this dissertation reaffirmed and expanded on an insight into the surveillance capitalist system of web-tracking: only a few actors account for a significant portion of tracking.
This better understanding of the adversary allows us to generalize results that require us to scale down, such as work that deeply focuses on the handful of powerful trackers.
This handful can be continuously monitored and reverse engineered at more detail than prior work, and understanding their impact on real users in an ethical way can be pursued more easily.

%More focused efforts to mitigate harms we know of. Anonymity sets and discriminated-against groups.

\section{Users First}

While this dissertation made strides toward providing univeillance for users, future work should emphasize strengthening the shortcomings of translating technical results to real change for people.
In particular, this work was conducted with a through-line of Internet measurement. 
While Internet measurement is a powerful tool to understand systems, it is not perfect.
This incurs restrictions of biased data sources that do not always reflect user experience and can make quantification easier when focused on service providers.
Users should be the primary concern of univeillant work. 
We should take deliberate steps to ensure that people are benefiting from what we do.

\end{comment}

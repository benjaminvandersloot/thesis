all: dissertation.pdf 

dissertation.pdf: dissertation.tex dissertation/*.tex papers/*/figures/* papers/*/paper/*.tex refs.bib headers/*.tex headers/*.cls
	pdflatex dissertation
	bibtex dissertation
	pdflatex dissertation
	pdflatex dissertation

.PHONY: open
open: dissertation.pdf
	open dissertation.pdf

.PHONY: clean
clean:
	rm -f dissertation.pdf *.aux *.blg *.bbl *.log *.tmp *.xref *.idv *.4og *.4ct *.lg *.4tc *.out *.toc *.loc *.lof *~ *.lot headers/*.aux

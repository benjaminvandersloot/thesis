At the root of all secure systems is a trusted element.
For the web, the trusted element that allows security properties to be built upon is the certificate ecosystem.
Certificates link domain names to cryptographic keys, allowing a user-agent to reason about the authenticity and confidentiality of the information being exchanged.
However, certificates have been mis-issued in the past: an attacker obtains a certificate linking a key they own with a domain that they do not.
This is a problem for everyone relying upon web security.
Users may be uncertian if the certificate they receive has been misissued, web admins do not know if the only certificates for their site are correctly issued, and certificate authorities can not know if they were tricked into misissuing a certificate.
Transparency into the certificate ecosystem can improve the trust we place in certificates.

Our work is the first to comparatively analyze different views of the certificate ecosystem to understand the extent of their views, and therefore utility in extending trust.
We find that the certificate transparency project from Google had made bounds forward in improving transparency, but failed to capture significant portions of non-web TLS servers.
Moreover, we find a significant shortcoming in IPv4 scanning in assessing the diversity of web servers.
We merge together complementary data sets of certificate transparency and Censys to make it easier for web admins to check for mis-issued cetificates.
These combined data sets remain searchable for free, and are still continuously synchronized four years after the completion of the initial project.

This chapter is adapted from a joint publication that first appeared in the 
Proceedings of the 2016 Internet Measurement Conference (IMC'16)~\cite{vandersloot2016towards}.

\section{Introduction}

Nearly all secure web communication takes place over HTTPS\@.
Both the underlying TLS protocol and the supporting certificate public key
infrastructure (PKI)
have been studied extensively over the past five years,
with questions ranging from understanding the behavior of
certificate authorities~\cite{EFFOberservatory,https13} to detecting
server-side vulnerabilities and tracking how quickly they are
mitigated~\cite{freakattack,matter_of_heartbleed,drownattack}.

Such measurements are difficult to conduct well, since
there is no comprehensive set of trusted certificates or of HTTPS websites---no
ground truth for studying this ecosystem.  Instead, researchers
have attempted to gain visibility into it using various fragmentary
perspectives---such as scanning the IPv4 address space~\cite{zmap13},
querying popular Alexa domains, passively monitoring network traffic~\cite{notarypaper},
and querying Certificate Transparency (CT) logs~\cite{certificate-transparency,tactical-secret}.
Each methodology provides an imperfect view of the world, yet there has been little
work to analyze how they differ or how they might be combined to
piece together a more comprehensive picture.

Consider, for example, the different perspectives provided by CT logs and the Censys search engine~\cite{censys},
two widely used sources of certificate data.  CT is designed to enable auditing of trusted certificates
by recording them in publicly verifiable logs.  While this may someday provide a complete view
of the certificate ecosystem, at present, publishing certificates to CT logs is voluntary in most cases.
In contrast, Censys provides a public database of certificates collected by actively scanning
the IPv4 address space and Alexa Top Million domains.
Although IPv4 scanning might seem to promise an exhaustive view of certificates in use on the
public Internet, it misses several important cases, including those served exclusively over IPv6.
IP-based scanning also cannot provide the TLS Server Name Indication (SNI) header~\cite{rfc6066},
which specifies the requested domain name and is necessary when a server hosts multiple sites from
a single IP address.

%Crawling DNS zones is a method we bring forward in this work, and is initially appealing
%as a means to obtaining all certificates in the crawled zones. However, we only know the
%domain names in the zone, not each of their subdomains. Each of these subdomains could
%have its own certificate.

%Moved this to this section to get it at the top of the next page
\input{papers/certs/figures/perspectives}

In this work, we comparatively analyze the certificates seen by eight measurement perspectives: (1)
a Censys certificate snapshot, (2) an exhaustive IPv4 scan on TCP/443,
(3) a scan of Alexa Top Million domains, (4) a snapshot of public CT Logs,
(5) a scan of domains contained in these CT logs, (6) a scan of domains
contained in the \texttt{.com}, \texttt{.net}, and \texttt{.org} zone files~\cite{zonefile},
(7) a scan of domains from the Common Crawl dataset~\cite{commoncrawl}, and (8) certificates passively
observed by the ICSI SSL Notary using passive network monitoring~\cite{notarypaper}.

Combining these datasets, we observe nearly 17~million unique browser-trusted
certificates that were valid during our measurement interval, August 29 to September 8, 2016.
Of these, 90.5\% appeared in public CT logs and 38.0\% were seen by Censys.
To understand this difference, we investigate the impact of SNI
by attempting connections to 30~million domains extracted from
certificates in CT logs.
Only 35\% of domains that accepted a connection with SNI offered the same certificate when
SNI was not used.  This places an upper bound on the certificates observable by IP-based scanning.
Combining data from Censys and CT covers 99.4\% of all trusted certificates
seen by any perspective we studied, and may closely approximate the public HTTPS ecosystem.
However, as the
vast majority of the certificates in our data originate from these two sources,
this number is suspect. To better validate the fraction of certificates visible
with these perspectives, we consider certificates seen by scanning domains from the
\texttt{.com}, \texttt{.net}, and \texttt{.org} zone files, and find that the
union of CT logs and Censys contains 98.5\% of them.

Based on these results, we recommend that researchers performing future HTTPS
measurements use a combination of data published in CT logs and Censys-style
IPv4 scanning.  To facilitate this, we are working with the operators of Censys
to implement synchronization between Censys and CT logs.  Going forward, Censys
will continuously incorporate certificate data from public CT logs in its
results and publish newly discovered certificates back to Google CT logs, making
either data source a strong foundation for studying the certificate ecosystem.

\section{Certificate Perspectives}

In order to compare techniques for measuring certificates, we
conducted six kinds of scans and analyzed two existing datasets.
Table~\ref{tab:perspectives} summarizes these perspectives, which we
describe in detail below.

This study is the first to comparatively analyze multiple passive and active datasets related to TLS.
With the current use of Internet Measurement to understand the scope and importance of 
vulnerabilities in TLS, it is vital to understand the biases and limitations of 
these datasets. As such, we use techniques common to TLS measurement generally.
We additionally pursue a few techniques that are, at time of original publication, unused in 
Internet Measurement of TLS.

\subsection{Perspectives Enumerated}

\paragraph{Certificate Transparency Logs}
Certificate Transparency (CT) aims to allow public auditing of trusted
certificates~\cite{certificate-transparency}.  Anyone can submit valid
certificates to CT log servers, which record them in
cryptographically verifiable public ledgers.  Although there is no universal requirement for submission, Google
records all certificates seen in its web crawls to CT logs.
Chrome requires all issuers submit extended validation (EV) certificates
to at least two logs~\cite{certificate-transparency-requirement}.
Chrome recently mandated that Symantec certificates signed after June 1,
2016, be submitted to be trusted as well~\cite{sustainingsecurity}.  Several CAs voluntarily
log all certificates they issue, notably Let's
Encrypt~\cite{letsencryptct} and StartCom~\cite{startcomct}.

We retrieved the certificates stored in twelve well-known
CT logs on September 8, 2016.  These logs are operated by Google
(``Pilot'', ``Aviator'', ``Rocketeer'', and ``Submariner''), Digicert,
StartCom, Izenpe, Symantec, Venafi, WoSign, CNNIC, and Shengnan GDCA\@.

\paragraph{Censys Certificate Snapshot} The Censys search engine~\cite{censys}
publishes daily snapshots of all the certificates it indexes.  Censys collects
certificates by exhaustively scanning the IPv4 address space without SNI,
and by connecting to all Alexa Top Million domains with SNI.
Our perspective is based on the September 8, 2016 snapshot.
%% and reflects the certificates that were visible in Censys at
%% that time.

\paragraph{Scan of FQDNs from CT\@}
We extracted the fully qualified domain names (FQDNs) from all certificates in
our CT log snapshots, covering the common name (CN) and subject alternative name
(SAN) fields. We then used ZGrab~\cite{censys} to attempt an HTTPS connection to each domain,
with SNI enabled.  The scan ran from the University of Michigan on August 29 and
September 6 and 8, 2016.

\paragraph{IPv4 HTTPS Scan} We used the ZMap suite~\cite{zmap13} to scan the IPv4 address space for
HTTPS servers listening on TCP/443.  The scan took place on August 29,
2016, from the University of Michigan.  For each listening host, we attempted a
TLS handshake and recorded the presented certificate chain. Since these connections
were based on IP addresses rather than domain names, they did not
include the SNI header.
%% , and we use the resulting data
%% to evaluate the certificates that are visible without the use of SNI\@.

\paragraph{Authoritative Zone Files} We attempted HTTPS handshakes with all
domains in the authoritative zone file~\cite{zonefile} for \texttt{.com},
\texttt{.net}, and \texttt{.org} domains, for both the base domain and the
\texttt{www} subdomain.  (Since the TLD zone files contain only the
name server entries for each domain, we learn only the base domain name.)
We ran these scans using ZGrab on August 29 and September 2, 2016,
from the University of Michigan. There were 153~million unique domains in
these zone files, and we completed 42~million successful HTTPS handshakes to
the base domains, and  40~million successful HTTPS handshakes to
the \texttt{www} subdomains. While we connected to many domains, the certificates
served were often only valid for the hosting provider's domain name and not
the scanned domain.

%, and none of the its subdomains.

\paragraph{Common Crawl} The Common Crawl project~\cite{commoncrawl} aims to
perform a regular, complete crawl of public websites.
We processed the January 2016 crawl and extracted 28.9~million unique domains.
We used ZGrab to attempt an HTTPS
connection to every domain on September 3, 2016,
from the University of Michigan.

\paragraph{Alexa Top Million HTTPS Scan} We used the ZMap suite to attempt
connections to the Alexa Top Million domains.  The scan took place on September
3, 2016, from the University of Michigan.  For each listening host, we attempted
a TLS handshake with SNI enabled and recorded the presented certificate chain\@.

\paragraph{ICSI SSL Notary}
The SSL Notary dataset consists of daily Internet traffic from approximately
180,000 users at five North American academic or research institutions~\cite{notarypaper}.
% Sorry - I am not allowed to say where the one outside is located.
We analyzed 2.2~billion TLS connections on TCP/443 from July 29 to August 29, 2016,
extracting a total of 635,314 certificates. We excluded incomplete connections
as well as HPC, Grid, and Tor certificates, resulting in 386,051~certificates, of which
256,869 were trusted by the Mozilla NSS root store.

Due to a nondisclosure agreement that limited our internal data sharing, Notary
certificates are not included in cases where we consider the union of all perspectives.
This reduces the size of the union by 0.02\%.


In total across all these perspectives, we discovered 17~million unique
certificates that were valid and trusted by the Mozilla NSS root store.  Since
the different datasets contain somewhat different temporal perspectives, we
consider certificates to be valid only if their date ranges cover our entire
collection period, August 29 to September 8. By constraining our data in this
way, we ensure that no data source contains certificates that would be invalid
in another data source due to the time when the certificates were validated.

\subsection{Ethics of Measurement}

For our active scanning, we honored the University of Michigan's institutional
blacklist to exclude endpoints that previously requested not to be scanned.
We also followed the best practices defined by Durumeric
et~al.\@~\cite{zmap13}; we refer to that work for more discussion of the ethics
of active scanning. Passive data collection was cleared by the responsible parties at
each contributing institution.  The ICSI SSL Notary stores connection metadata
(e.g., certificate and cipher information) without collecting any connection
payload.




\section{Impact of Perspective on HTTPS Research}


A number of recent studies have used IPv4 and Alexa Top Million scanning to
measure how HTTPS is deployed in the wild~\cite{freak,https13,EFFOberservatory,
Lenstra12,drownattack,freakattack,censys,matter_of_heartbleed,wake_of_heartbleed,inconvenient}.
Our finding that IPv4 scans miss nearly two-thirds of certificates suggests
that, if these studies were performed today, they might not accurately reflect
the state of the Internet.

\input{papers/certs/figures/freak}

To provide a concrete example of the differences caused by different
perspectives, we present a survey of sites vulnerable to the FREAK
attack~\cite{freakattack} in Table~\ref{tab:freak}. The data comes from
scanning each of: the IPv4 address space, CT log FQDNs, Alexa Top Million domains, our
zone files, and domains extracted from Common Crawl. For each set, we measure how many
responsive hosts are vulnerable.

The number of vulnerable hosts changes with each perspective we measure.
The vulnerability rates range from 1.88\% of IPs vulnerable when measured by IPv4 scanning,
to 4.54\% of IPs vulnerable when measured by our Common Crawl scan.
This variation demonstrates the necessity of considering the perspective used when performing measurements.

\input{papers/certs/figures/issuers}

We also observe differences when comparing the most common certificate issuers
seen in each perspective, as shown in Table~\ref{tab:issuers}.  We measure this
by grouping certificates by their issuer organization field and manually
deduplicating similar issuer names. Different perspectives display differing
views on which CA is most popular. For example, Let's Encrypt is the most
popular CA in CT, Common Crawl, and zone scans, but it is ranked fifth in IPv4
scans.

\section{Two Complementary Perspectives Emerge}

Our certificate ``universe'' consists of 16,989,236 unique, valid certificates from our eight
perspectives. These certificates contain 32,454,062 FQDNs from
12,673,515 sites (as defined by the public suffix list~\cite{publicsuffix}).
The CT logs and Censys snapshot are the two
largest datasets. The CT logs contain 15,374,936 certificates (90.5\% of
certificates observed in this study).  Censys sees  6,448,588~certificates---38\% coverage of the
certificates observed in this study. When combined, these two perspectives
provide 99.4\% coverage of all certificates we observe and 99.7\% coverage of
sites.

\begin{table}
\input{papers/certs/figures/ct_missing}
	\vspace{10pt}
\input{papers/certs/figures/ip_missing}
	\vspace{10pt}
\input{papers/certs/figures/sni}
	\vspace{-5pt}
\end{table}

\subsection{Limitations of Censys}

\begin{table*}
	\input{papers/certs/figures/alexa}
    \vspace{10pt}
    \input{papers/certs/figures/union}
    \vspace{10pt}
    \input{papers/certs/figures/arch_zone}
\end{table*}

The Censys snapshot only covers 38\% of our certificate
universe.  Two sources are responsible for approximately 64\% of the missing certificates.
As shown in Table~\ref{tab:ip_missing}, 90.8\% of Let's Encrypt certificates are
absent from Censys, accounting for 42\% of the certificates missed by Censys and
23.5\% of the certificate universe. Let's Encrypt submits all issued
certificates to CT, but it appears that many of these certificates are
inaccessible without SNI or are not served on public sites.

CloudFlare accounts for 17\% of all certificates in this study, but Censys
misses 81\% of these, resulting in an exclusion of 13.9\% of the certificate
universe. We manually confirmed that the vast majority of CloudFlare
certificates are only accessible through SNI\@. This intuitively makes sense
because most Censys certificates are found through IPv4 scans that do not
include SNI information.

\subsubsection{Server Name Indication}
\label{sec:certs_sni}

In order to directly measure the impact of SNI, we performed two
scans over all FQDNs contained in valid certificates in the CT logs. We scanned 30
million domain names, and
%% after we resolve DNS, apply the blacklist, and
%% attempt connections, we
were able to complete
successful HTTPS handshakes with 68\% using SNI\@.
%It's not immediately clear whether these certificates were ever accessible,
%had recently been switched, or were only accessible internally within an
%organization.
As shown in Table~\ref{tab:sni}, only 77\% of domains that accepted a connection made with SNI
accepted connections without it, and only 35\% returned the same certificate as when SNI was used.
This further shows that scanning without SNI misses a substantial fraction of websites.

In order to understand if this discrepancy applies to commonly visited sites,
we can limit the scope of our comparison to certificates discovered through
Alexa Top Million scanning, as shown in Table~\ref{tab:alexa}. Even for
these popular sites, IP-based scanning misses 27\% of certificates and 65\% of sites, due
to a lack of SNI---a massive difference compared to the 0.7\% that Durumeric
et~al.\ found in 2013~\cite{https13}.
Notably, scans of CT and IPv4 combined provide 98.5\% of the certificates presented
in our Alexa Top Million scan.


\subsection{Limitations of Certificate Transparency}

While Certificate Transparency is large, and eventual requirement by major browsers
seem to make it an authoritative source for certificates on the Internet, this is not the
case. We find that there are significant short-comings in its coverage. In particular,
we find that non-web use of TLS is missing in significant quantity and that some certificate
authorities are slow to adopt Certificate Transparency.


\subsubsection{Web Focused}

We also find that CT is skewed towards web content and away from other
TLS-based services, such as webmail, that might not be linked to by websites Google crawls.
For example, we find that CT misses 20.8\% of certificates with the subdomain
\texttt{mail} and 50.8\% of certificates with a subdomain containing \texttt{vpn}. In
contrast, CT only misses 4.3\% of certificates with the subdomain \texttt{www}.

\subsubsection{CA Bias Participation}

While CT is by far the largest perspective, it still misses 9.5\% of the
certificate universe, including 29.6\% of GoDaddy certificates and 28.4\% of
cPanel certificates, as shown in Table~\ref{tab:ct_missing}. None of the CAs in
the table submit domain validated (DV) certificates to public logs.
In contrast, CT captures 99.3\% of CloudFlare certificates and
100\% of Let's Encrypt certificates.

\section{Passive Traffic Monitoring}

Passive measurement has many properties that make it different than the other 
methods used in this chapter. It is the most realistic representation of the Internet's
certificates as percieved by users because it is driven by users. However, this view 
is also less complete than other active methods. This is because all certificates
we observe must be requested by a user in our study sample during our measurement 
period.

\subsection{Limitations of Scope}

The ICSI Notary is the largest passive measurement infrastructure in use for collecting 
TLS behavior in open publications. It analyzed the traffic of 180,000 users at the time
of our study, and over our one month of measurement (longer than any active method) it
observed 2.2 billion TLS connections. In spite of this the ICSI Notary was the smallest
dataset of certificates collected. A full month of collection was required to very nearly 
achieve a single Alexa-based scan, although it did produce 3,805 unique certificates.
This indicates significant limitations of passive measurement with respect to providing 
a view into a significant fraction of the Internet and limitations of active measurement in
providing insight into user experience online.

\subsection{User-driven}

The ICSI Notary perspective is derived from passive monitoring of network
traffic. It only includes certificates actually seen on the wire, and
therefore differs significantly from our other perspectives.
%% Additionally, this
%% perspective contains per-connection meta-data that allows us to gauge
%% how HTTPS is used in practice at the participating institutions.

In contrast to our active scans, passive monitoring contains certificates
from IPv6 connections. We encountered 822,338 server IP addresses in our Notary
dataset.  Of these, 8.2\% (67,725) were IPv6 addresses, comprising
13\% of the observed connections.
%% While IPv6 might become significant for
%% future measurements, ignoring during active measurements does not currently
%% affect outcomes in a significant way.
There were 4,512 certificates that were only encountered on IPv6
addresses, but only 218 of them were not observed in any other perspective.  This
suggests that IPv6 does not impact conclusions drawn from scanning
significantly, but as our passive dataset is relatively restricted, further measurement
is necessary to verify this claim.
Underscoring the importance of SNI discussed in
Section~\ref{sec:certs_sni}, only 9.7\% of connections in the Notary
dataset did not use SNI\@. In total, we saw 3,246,725 unique SNI values.


The Notary saw only 3,805 certificates that were not observed by any other
perspective. We believe these are due to certificate
changes during the longer passive measurement interval. This is supported by
the fact that only 34\% of the certificates were encountered at all during the last
week of the measurement interval. Furthermore, 75\% were
issued by CloudFlare, which rotates certificates quickly.
%% For the
%% remaining certificates not explained by this theory, we believe the notary saw
%% unique domains not contained in our other datasets.

There were 68,700 certificates seen by our passive measurement that were not present in
Censys.  They have a similar composition to the certificates in CT logs that Censys missed. 39\%
are issued by Let's Encrypt, which requires sites to frequently
re-issue certificates.  20\% are attributable to Wordpress-hosted
blogs and 13\% to CloudFlare, services that heavily depend on SNI
and would therefore not be seen by IPv4 scans.

\section{Merger of Certificate Transparency and Censys}

As Table~\ref{tab:union} shows, combining data from CT logs and Censys yields
99.4\% coverage of all certificates observed by any of our perspectives and
99.7\% of all FQDNs and all sites.  However, since these perspectives are also
our two largest data sources, this statistic may be artificially inflated.

Fortunately, scanning all domains in the \texttt{.com}, \texttt{.net}, and \texttt{.org}
zone file provides us with ground truth for all sites being served on the root
and \texttt{www} subdomain in those zones. We can use this to understand what
coverage each perspective gives in these zones. While the zones do not contain
all subdomains or even all domain names on the Internet, they are a large
subset: 153~million unique domains. We compare Censys data and CT logs over
the certificates obtained from the zone scans in Table~\ref{tab:arch_zone}.

Of these certificates, we find 98.5\% are obtained through
either Censys or CT logs. Since this is smaller than the corresponding percentage
for all certificates we observe, there are likely certificates
being hosted in other zones and in other subdomains not observed by any
method we use. Therefore, the coverage of IPv4 scans and CT logs on
the entire population of certificates on the Internet is overestimated
by Table~\ref{tab:union}.

Conversely, the coverage of Censys scanning on the zone dataset is increased to
54\%, from 38\% over all certificates observed. This is potentially due to
certificates in CT logs that are not actively hosted on the Internet (e.g.,
intranet sites). As a result, the Censys coverage of the entire population of
certificates on the Internet is underestimated by Table~\ref{tab:union}.

\subsection{Benefits to System Administrators}

This merger makes it easier for system administrators to keep up to date with the
full set of certificates issued for their domains. If the administrator were 
previously monitoring Certificate Transparency logs, they are now also monitoring
those accessed via IPv4 scanning without any change in method. Similarly, if 
system administrators can now track certificates from both of our largest datasets
through free search tools, such as those provided by Censys and \texttt{crt.sh}.
This allows end users to more reasonably trust that a valid certificate they 
receive is not misissued.

\subsection{Load-testing Certificate Transparency}

During our initial upload of Censys certificates to Google's certificate transparency logs,
we proceeded with a single core uploading certificates as fast as the log servers would accept them.
This, incidentally, was near the limit of certificates per day that the production log servers were
able to handle while maintaining service guarantees of maximum merge delay (MMD). This led to the discovery that
the production CT logs were not behind a rate limiting HTTP proxy, nor did they perform any rate limiting of their own.

As a result, our initial upload caused the Aviator log to exceed its MMD. This violation
of the Certificate Transparancy service guarantee left it untrusted by browsers, and 
it therefore ceased operation. 

This accidental discovery caused Google to ensure the remaining Certificate 
Transparency logs were placed behind rate limiting proxies with rates set 
low enough that the MMD can be met. Additionally, more logs have been spun up. 
As a result, the Certificate Transparency ecosystem is more robust as a result
of our modest load test.

\section{Related Work}

This work was inspired by a large body of research focusing on the HTTPS
ecosystem and supporting
PKI~\cite{freak,https13,EFFOberservatory,drownattack,freakattack,censys,matter_of_heartbleed,wake_of_heartbleed,inconvenient}.
These works have run the gamut of HTTPS measurement, ranging from the
certificate authority ecosystem~\cite{EFFOberservatory,https13} to cryptographic
keys generated without entropy~\cite{miningpqs}, and how operators react
to vulnerabilities~\cite{matter_of_heartbleed}.

In 2010, the EFF launched the SSL Observatory~\cite{EFFOberservatory}, in which they
performed a scan of the IPv4 address space over a three month period in order to
identify trusted certificate authorities. Later, in 2011, Vratonjic
et~al.~\cite{inconvenient} crawled the Alexa Top Million finding that only 5.7\%
of websites correctly deploy HTTPS. The same year, Holz
et~al.~\cite{ssllandscape} carried out a similar study, combining active
measurements of the Top Million from from several vantage points, with data from
passive measurement at a large research institution. They briefly compared the
differences of their vantage points, concentrating on differences caused by scan
origins.

In 2013, Durumeric et~al.~\cite{https13} analyzed the state of the HTTPS PKI by
repeatedly scanning the IPv4 address space. They briefly considered how much of
the Alexa Top Million was only accessible using SNI, finding
that, at that time, SNI was not widely required.  Other studies use data based
on websites on the Top Million~\cite{Huang2014}, and scans of the IPv4
ecosystem~\cite{miningpqs,attributecorrelation,matter_of_heartbleed}. Studies
have also been based on combinations of Alexa sites, random domains, known
phishing domains data from passive network
monitoring~\cite{heresmycert,noattack,tangledmass,Mishari09,matter_of_heartbleed}
Most recently, studies have also investigated TLS deployment outside of
HTTPS~\cite{DurumericMail,HolzMail}.

Our work does not focus on the questions asked by these individual studies, but
instead focuses on \emph{how} these types of studies should be measuring the
HTTPS ecosystem.  We hope that by better validating different methodologies for
studying the HTTPS PKI, we can help future papers obtain more accurate
measurements.


\section{Conclusion}


Over the past five years, dozens of studies have measured the HTTPS ecosystem
and supporting PKI. Unfortunately, without a clear ground truth, these studies
pieced together a view built on a series of fractured and imperfect
methodologies. In this work, we investigated these methodologies, finding that
IPv4 enumeration no longer provides a representative view of how TLS servers are
configured, due to SNI deployment. IPv4 scans miss more than two-thirds of valid
certificates and associated measurements can differ dramatically from site-based
approaches. Certificate Transparency provides a new perspective, which finds
90.5\% of certificates observed in this study, but is skewed towards a few
authorities that submit the certificates they issue. We find that a more
comprehensive yet readily accessible methodology is to use a combination of CT
and Censys data, which together account for 99.4\% of our observed certificates.
To this end, we are working with the Censys team to implement continuous
certificate synchronization between Censys and Google's CT logs, which will soon
make either data source a nearly comprehensive view of trusted HTTPS
certificates.


